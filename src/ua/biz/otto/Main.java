package ua.biz.otto;

public class Main {

    public static void main(String[] args) {

        // Training

        //1
        int i = 5;
        System.out.println(i + "\n");

        //2
        int s = 6;
        s += 2;
        System.out.println(s + "\n");

        //3
        int a = 9;
        a -= 4;
        System.out.println(a + "\n");

        //4
        int u = 3;
        u *= 6;
        System.out.println(u + "\n");

        //5
        long l = 3000000;
        l /= 2;
        System.out.println(l + "\n");

        //6
        long e = 7;
        e %= 5;
        System.out.println(e + "\n");

        //7
        float q = 9.3f;
        q /= 3;
        q += 6;
        System.out.println(q + "\n");

        //8
        double r = 8.9d;
        r -= 5;
        r *= 6;
        System.out.println(r + "\n");

        //9
        int t = 6;
        System.out.println(t += 1);
        System.out.println(t++);
        System.out.println(++t + "\n");

        //10
        int o = 6;
        System.out.println(o -= 1);
        System.out.println(o--);
        System.out.println(--o + "\n");


        // Countries population

        long Belgium_population = 11094850;
        long Bulgaria_population = 7364570;
        long Cyprus_population = 862011;
        long Czech_population = 10505445;
        long Denmark_population = 5573894;
        long Estonia_population = 1294486;

        System.out.println("Belgium population " + " = " + Belgium_population + " people");

        System.out.println("Bulgaria population " + " = " + Bulgaria_population + " people");

        System.out.println("Cyprus population " + " = " + Cyprus_population + " people");

        System.out.println("Czech population " + " = " + Czech_population + " people");

        System.out.println("Denmark population " + " = " + Denmark_population + " people");

        System.out.println("Estonia population " + " = " + Estonia_population + " people");

        System.out.println('\n');


        // Countries square

        long Bulgaria = 110910;
        long Czech = 78866;
        long Denmark = 42895;
        long Estonia = 45226;

        System.out.println("Bulgaria square = " + Bulgaria + "km\u00B2" + "=" + Bulgaria * 1000 + "m\u00B2" + "=" + Bulgaria / 1.609344 + "mi\u00B2");

        System.out.println("Czech square = " + Czech + "km\u00B2" + "=" + Czech * 1000 + "m\u00B2" + "=" + Czech / 1.609344 + "mi\u00B2");

        System.out.println("Denmark square = " + Denmark + "km\u00B2" + "=" + Denmark * 1000 + "m\u00B2" + "=" + Denmark / 1.609344 + "mi\u00B2");

        System.out.println("Estonia square = " + Estonia + "km\u00B2" + "=" + Estonia * 1000 + "m\u00B2" + "=" + Estonia / 1.609344 + "mi\u00B2");

        System.out.println('\n');


        // Currency exchange

        float usd = 26.2727f;
        float eur = 32.4231f;
        float rub = 0.4608f;
        int uah = 5000;

        System.out.println("5000 uah = " + uah / usd + "usd=" + uah / eur + "eur=" + uah / rub + "rub");


    }
}
